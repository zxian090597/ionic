const mongoose = require('mongoose')

var student=new mongoose.Schema({
    name:String,
    ic:String,
    matric:String,
    faculty:String,
    program:String,
    year:String,
    pa:String,
    sc:String,
    Result_ID:String,
    
})

var result=new mongoose.Schema({
    Course:
    [{
        code:String,
        course:String,
        grad:String,
        pointer:String,
        CH:Number,
        jmn:Number,

    }]
})

mongoose.model('student',student,'student');
mongoose.model('result',result,'result');

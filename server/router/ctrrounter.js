const express = require('express'),
ctr = require('./routes'),

router = express.Router()
router.get('/api/getname',ctr.getname)
router.get('/api/getresult/:id',ctr.getresult)

module.exports = router;
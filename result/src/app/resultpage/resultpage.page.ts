import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ServiceService } from '../service.service';
import * as html2pdf from "html2pdf.js"

@Component({
  selector: 'app-resultpage',
  templateUrl: './resultpage.page.html',
  styleUrls: ['./resultpage.page.scss'],
})
export class ResultpagePage implements OnInit {
  
  id: any;
  results:any
  student:any 
  
  constructor(private actRoute: ActivatedRoute,private router: Router, public Service: ServiceService) { 
    this.student = {};
    this.results = [];
    this.id = this.actRoute.snapshot.paramMap.get('id');
    console.log(this.id)
    this.ngOnInit()
  }
 
  ngOnInit() {
    this.getresult(this.id);
    console.log('init')
  }

  ionViewDidLoad() {
    this.ngOnInit()
   
  }
  
  getresult(id){
    this.Service.getresult(id)
    .then((res) => {
        console.log("get data" + res)
        this.results = res['coursedata']["Course"]
        console.log(this.results)
        this.student = res["student"]
      }
    )
    console.log(this.student)
    console.log("some")
  }


  createPdf(){
    const option ={
      filename:'result.pdf',
      image:{type:'jpg'},
      html2canvas:{},
      jsPDF:{format: 'letter', orientation: 'portrait' }
    };

    const content: Element=document.getElementById('printable-area');

    html2pdf()
    .from(content)
    .set(option)
    .save();
  }
    

}

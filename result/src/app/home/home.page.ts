import { Component } from '@angular/core';
import { ServiceService } from '../service.service';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  students:any
  


  constructor(private Service: ServiceService) {
    this.ionViewDidLoad();
  }

  ionViewDidLoad(){

    this.Service.getname().then((data) => {
      console.log(data);
      this.students = data;
      
    });

  }

 
}

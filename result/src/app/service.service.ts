import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  
  constructor(private http:HttpClient) { }

  getname(){
    return this.http.get('http://localhost:3000/api/getname').toPromise();

  }

  
  getresult(_id){
    return this.http.get("http://localhost:3000/api/getresult/"+ _id).toPromise();
  }
}

